# SystoonGatewayUi

This is my last time to writing code personally.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.
## Prerequisites
Both the CLI and generated project have dependencies that require Node 6.9.0 or higher, together with NPM 3 or higher.

## Installation
**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)
```bash
npm install -g @angular/cli
npm install -g json-server ( Project use json-server to mock the data )
```

## Usage

### Launch the json-server
```
json-server mock-data.js -w
```
Navigate to `http://localhost:3000/users`, choose a user and remember the employee id and password for the next authentication. 

### Launch the application
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. Input the employee id and password for the authentication. The app will automatically reload if you change any of the source files.


