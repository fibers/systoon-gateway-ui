import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {LoginService} from './login/login.service';
import {Log} from 'ng2-logger';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  private log = Log.create(this.constructor.name);

  constructor(private loginService: LoginService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.log.info('canActivate', next, state);

    const url: string = state.url;

    if (this.loginService.isLoggedIn) {
      return true;
    }

    this.loginService.redirectUrl = url;

    this.router.navigate(['/login']);
    return false;

  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    | Promise<boolean>
    | boolean {

    this.log.info('canActivateChild', childRoute, state);

    return this.canActivate(childRoute, state);
  }

}
