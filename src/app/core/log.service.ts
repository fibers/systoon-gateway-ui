import {Injectable} from '@angular/core';
import {Log} from 'ng2-logger';
import {environment} from '../../environments/environment';

@Injectable()
export class LogService {

  log;

  constructor() {
    if (environment.production) {
      Log.setProductionMode();
    }

    this.log = Log.create('SystoonGateway');
    this.log.color = 'blue';
  }

  d(message: string, ...otherParams: any[]) {
    this.log.data(message, otherParams);
  }

  w(message: string, ...otherParams: any[]) {
    this.log.warn(message, otherParams);
  }

  i(message: string, ...otherParams: any[]) {
    this.log.info(message, otherParams);
  }

  e(message: string, ...otherParams: any[]) {
    this.log.error(message, otherParams);
  }

}
