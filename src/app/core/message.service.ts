import {Injectable} from '@angular/core';
import {NzMessageService} from 'ng-zorro-antd';
import {NzMessageDataOptions} from 'ng-zorro-antd/src/message/nz-message.definitions';
/**
 * Global Message notification
 */
@Injectable()
export class MessageService {

  private options: NzMessageDataOptions;

  constructor(private messageService: NzMessageService) {

    this.options = {
      nzDuration: 3000,
      nzAnimate: true,
      nzPauseOnHover: true,
    };
  }

  info(message: string): void {
    this.messageService.info(message, this.options);
  }

  success(message: string): void {
    this.messageService.success(message, this.options);
  }

  error(message: string): void {
    this.messageService.error(message, this.options);
  }

  warning(message: string): void {
    this.messageService.warning(message, this.options);
  }
}
