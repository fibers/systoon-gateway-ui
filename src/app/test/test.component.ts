import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  parentValue: number;
  childValue: number;

  list = [1, 2, 3, 4, 5];

  constructor() {
  }

  ngOnInit() {
    this.parentValue = 1;
    this.childValue = this.parentValue - 1;
  }

  onClick() {
    this.parentValue += 2;

    this.list.push(6);
  }

}
