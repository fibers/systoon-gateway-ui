import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {

  topmenuName: string;
  submenuName: string;

  constructor(private router: Router,
              private location: Location) {
  }

  ngOnInit() {
    this.setBreadcrumb();

    this.router.events.subscribe(_ => {
      if (_ instanceof NavigationEnd) {
        this.setBreadcrumb();
      }
    });
  }

  setBreadcrumb(): void {
    const path = this.location.path();
    const pathArray: string [] = path.substring(1).split('/');

    if (pathArray.length >= 2) {
      this.submenuName = pathArray[1];
      this.topmenuName = pathArray[0];
    } else if (pathArray.length === 1) {
      this.topmenuName = pathArray[0];
    } else {
      this.topmenuName = 'Home';
    }
  }

}
