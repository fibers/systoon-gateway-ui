import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {VersionDetailComponent} from './versions/version-detail/version-detail.component';
import {VersionListComponent} from './versions/version-list/version-list.component';
import {ServiceDetailComponent} from './services/service-detail/service-detail.component';
import {ServiceListComponent} from './services/service-list/service-list.component';
import {ApiListComponent} from './apis/api-list/api-list.component';
import {ApiDetailComponent} from './apis/api-detail/api-detail.component';
import {VersionMappingDetailComponent} from './version-mappings/version-mapping-detail/version-mapping-detail.component';
import {VersionMappingListComponent} from './version-mappings/version-mapping-list/version-mapping-list.component';

const apiManageRoutes: Routes = [
  {
    path: 'versions',
    children: [
      {path: '', component: VersionListComponent},
      {path: 'create', component: VersionDetailComponent},
      {path: 'edit/:id', component: VersionDetailComponent},
      {path: '**', redirectTo: ''}
    ]
  },
  {
    path: 'services',
    children: [
      {path: '', component: ServiceListComponent},
      {path: 'create', component: ServiceDetailComponent},
      {path: 'edit/:id', component: ServiceDetailComponent},
      {path: '**', redirectTo: ''}
    ]
  },
  {
    path: 'apis',
    children: [
      {path: '', component: ApiListComponent},
      {path: 'create', component: ApiDetailComponent},
      {path: 'edit/:id', component: ApiDetailComponent},
      {path: '**', redirectTo: ''}
    ]
  },
  {
    path: 'version-mappings',
    children: [
      {path: '', component: VersionMappingListComponent},
      {path: 'create', component: VersionMappingDetailComponent},
      {path: 'edit/:id', component: VersionMappingDetailComponent},
      {path: '**', redirectTo: ''}
    ]
  },
  {path: '', redirectTo: 'versions', pathMatch: 'full'},
];

@NgModule({
  imports: [
    RouterModule.forChild(apiManageRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ApiManageRoutingModule {
}
