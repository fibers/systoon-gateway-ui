import {BaseModel} from '../../base/base.model';
export class VersionModel extends BaseModel {
  publishVersion: string;
  description: string;
}
