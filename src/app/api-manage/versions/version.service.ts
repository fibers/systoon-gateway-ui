import {Injectable} from '@angular/core';
import {BaseService} from '../../base/base.service';
import {HttpClient} from '@angular/common/http';
import {VersionModel} from './version.model';

@Injectable()
export class VersionService extends BaseService<VersionModel> {

  constructor(http: HttpClient) {
    super('api/versionInfos', http);
  }

}
