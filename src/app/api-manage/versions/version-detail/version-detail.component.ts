import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {Log} from 'ng2-logger';

import {MessageService} from '../../../core/message.service';
import {VersionService} from '../version.service';
import {VersionModel} from '../version.model';

@Component({
  selector: 'app-version-detail',
  templateUrl: './version-detail.component.html',
  styleUrls: ['./version-detail.component.scss']
})
export class VersionDetailComponent implements OnInit {

  private log = Log.create(this.constructor.name);

  form: FormGroup;
  formTitle = '';
  isSaveBtnLoading = false;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private msg: MessageService,
              private versionService: VersionService) {
  }

  /**
   * Initialize the FormGroup object and load the data.
   */
  ngOnInit() {
    this.log.info('ngOnInit', this.route.snapshot.paramMap);

    this.form = this.fb.group({
      id: [null, Validators.nullValidator],
      publishVersion: [null, [Validators.required, Validators.pattern('^v[0-9]+\.[0-9]+$')]],
      description: [null, [Validators.required]]
    });

    const id = +this.route.snapshot.paramMap.get('id');

    if (id) {
      this.formTitle = 'Edit';
      this.versionService.get(id).subscribe(
        data => {
          this.form.patchValue(data);
        },
        err => {
          this.log.error('ngOnInit', err);
          this.msg.error('Failed to load the form data.');
        }
      );
    } else {
      this.formTitle = 'Create';
    }
  }

  /**
   * Submit the form data to remote server and navigate to the list page.
   * @param versionModel The versionModel object
   */
  submitForm(versionModel: VersionModel) {
    this.log.info('submitForm', versionModel);

    this.isSaveBtnLoading = true;

    if (versionModel.id) {
      this.versionService.update(versionModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    } else {
      this.versionService.create(versionModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    }
  }

  /**
   * Close the form and navigate to the list page.
   */
  closeForm() {
    this.log.info('closeForm');
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  /**
   * The abbreviated word for form.controls.
   * @returns {{[p: string]: AbstractControl}} The form.controls object.
   */
  public get fc() {
    return this.form.controls;
  }

}
