import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionMappingDetailComponent } from './version-mapping-detail.component';

describe('VersionMappingDetailComponent', () => {
  let component: VersionMappingDetailComponent;
  let fixture: ComponentFixture<VersionMappingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionMappingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionMappingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
