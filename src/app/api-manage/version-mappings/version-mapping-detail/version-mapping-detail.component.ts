import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Log} from 'ng2-logger';

import {MessageService} from '../../../core/message.service';
import {ServiceService} from '../../services/service.service';
import {VersionMappingService} from '../version-mapping.service';
import {VersionService} from '../../versions/version.service';
import {ApiService} from '../../apis/api.service';
import {VersionMappingModel} from '../version-mapping.model';

@Component({
  selector: 'app-version-mapping-detail',
  templateUrl: './version-mapping-detail.component.html',
  styleUrls: ['./version-mapping-detail.component.scss']
})
export class VersionMappingDetailComponent implements OnInit {

  private log = Log.create(this.constructor.name);

  form: FormGroup;
  formTitle = '';
  isEditMode = true;
  isSaveBtnLoading = false;

  formOptions = {
    publishVersion: {
      loaded: false,
      options: []
    },
    serviceId: {
      loaded: false,
      options: []
    },
    apiName: {
      loaded: false,
      options: []
    },
    apiVersion: {
      loaded: false,
      options: []
    },
    openScope: {
      loaded: false,
      options: []
    }
  };

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private msg: MessageService,
              private versionMappingService: VersionMappingService,
              private versionService: VersionService,
              private serviceService: ServiceService,
              private apiService: ApiService) {
  }

  /**
   * Initialize the FormGroup object and load the data.
   */
  ngOnInit() {
    this.log.info('ngOnInit', this.route.snapshot.paramMap);

    this.form = this.fb.group({
      id: [null, Validators.nullValidator],
      publishVersion: [null, [Validators.required, Validators.pattern('^v[0-9]+\.[0-9]+$')]],
      serviceId: [null, [Validators.required]],
      apiName: [null, [Validators.required]],
      apiVersion: [null, [Validators.required, Validators.pattern('^v[0-9]+$')]],
      openScope: [null, [Validators.required]]
    });

    const id = +this.route.snapshot.paramMap.get('id');

    if (id) {
      this.formTitle = 'Edit';
      this.isEditMode = true;
      this.versionMappingService.get(id).subscribe(
        data => {
          this.form.patchValue(data);
        },
        err => {
          this.log.error('ngOnInit', err);
          this.msg.error('Failed to load the form data.');
        }
      );
    } else {
      this.formTitle = 'Create';
      this.isEditMode = false;
    }
  }

  /**
   * Load the option list in real-time
   * @param optionName The option name.
   * @param isOpen  The status of the current options
   */
  optionsOpen(optionName, isOpen) {

    this.log.info('optionsOpen', optionName, isOpen);

    if (isOpen) {
      switch (optionName) {
        case 'publishVersion': {
          this.versionService.list().subscribe(
            data => {
              this.formOptions[optionName].options = data.map(item => item.publishVersion);
              this.formOptions[optionName].loaded = true;
            },
            err => {
              this.log.error('optionsOpen', err);
              this.msg.error('Failed to load publish version list');
            }
          );
          break;
        }
        case 'serviceId': {
          this.serviceService.list().subscribe(
            data => {
              this.formOptions[optionName].options = data.map(item => item.serviceId);
              this.formOptions[optionName].loaded = true;
            },
            err => {
              this.log.error('optionsOpen', err);
              this.msg.error('Failed to load service id list');
            }
          );
          break;
        }
        case 'apiName': {
          this.apiService.getApiInServiceId(this.form.controls.serviceId.value).subscribe(
            data => {
              this.formOptions[optionName].options = data.map(item => item.apiName);
              this.formOptions[optionName].loaded = true;
            },
            err => {
              this.log.error('optionsOpen', err);
              this.msg.error('Failed to load version list');
            }
          );
          break;
        }
        case 'apiVersion': {
          this.apiService.getApiByName(this.form.controls.apiName.value).subscribe(
            data => {
              this.formOptions[optionName].options = data.map(item => item.apiVersion);
              this.formOptions[optionName].loaded = true;
            },
            err => {
              this.log.error('optionsOpen', err);
              this.msg.error('Failed to load version list');
            }
          );
          break;
        }
        case 'openScope': {
          // TODO: fake the openScope remote call.
          this.formOptions[optionName].options = [1, 2, 3];
          this.formOptions[optionName].loaded = true;

          break;
        }
        default: {
          this.log.warn('optionsOpen', 'No condition matched');
          break;
        }
      }

    } else {
      this.formOptions[optionName].loaded = false;
      this.formOptions[optionName].options = [];
    }
  }

  /**
   * Submit the form data to remote server and navigate to the list page.
   * @param versionMappingModel The versionMappingModel object
   */
  submitForm(versionMappingModel: VersionMappingModel) {
    this.log.info('submitForm', versionMappingModel);

    this.isSaveBtnLoading = true;

    if (versionMappingModel.id) {
      this.versionMappingService.update(versionMappingModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    } else {
      this.versionMappingService.create(versionMappingModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    }
  }

  /**
   * Close the form and navigate to the list page.
   */
  closeForm() {
    this.log.info('closeForm');
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  /**
   * The abbreviated word for form.controls.
   * @returns {{[p: string]: AbstractControl}} The form.controls object.
   */
  public get fc() {
    return this.form.controls;
  }
}
