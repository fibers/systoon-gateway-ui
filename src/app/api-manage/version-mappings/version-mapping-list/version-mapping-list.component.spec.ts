import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionMappingListComponent } from './version-mapping-list.component';

describe('VersionMappingListComponent', () => {
  let component: VersionMappingListComponent;
  let fixture: ComponentFixture<VersionMappingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionMappingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionMappingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
