import {Component, OnInit} from '@angular/core';

import {Log} from 'ng2-logger';
import * as _ from 'lodash';

import {MessageService} from '../../../core/message.service';
import {VersionMappingService} from '../version-mapping.service';
import {VersionMappingModel} from '../version-mapping.model';
import {VersionService} from '../../versions/version.service';

@Component({
  selector: 'app-version-mapping-list',
  templateUrl: './version-mapping-list.component.html',
  styleUrls: ['./version-mapping-list.component.scss']
})
export class VersionMappingListComponent implements OnInit {

  private log = Log.create(this.constructor.name);

  pageSizeSelectorValues = [10, 20, 50];
  pageIndex = 1;
  pageSize = 10;
  isTableLoading = true;

  totalData = {};
  copyData = [];
  data = [];
  displayData = [];

  isSelectIndeterminate = false;
  isSelectAll = false;
  selectCount = 0;
  isDeleteBtnDisabled = true;
  isDeleteBtnLoading = false;

  sortOptions = {
    id: 'ascend',
    publishVersion: null,
    serviceId: null,
    apiName: null,
    apiVersion: null,
    openScope: null,
    createTime: null,
    updateTime: null
  };

  sortName = 'id';
  sortValue = 'ascend';

  filterOptions = {
    operator: []
  };
  filterValue = {
    operator: []
  };
  isClearFilterBtnDisabled = true;

  selectedVersion = '';
  allVersions = [];

  constructor(private msg: MessageService,
              private versionMappingService: VersionMappingService,
              private versionService: VersionService) {
  }

  /**
   * Load the data.
   */
  ngOnInit() {
    this.log.info('ngOnInit');

    this.versionService.list().subscribe(
      data => {
        this.allVersions = _(data).map('publishVersion').uniq().sort().value();
        this.selectedVersion = this.allVersions[0];
      },
      err => {
        this.log.error('ngOnInit', err);
        this.msg.error('Failed to load the version.');
      }
    );

    this.loadData();
  }

  /**
   * Called when the current page's data changes.
   * @param data The current page's data
   */
  onDataChange(data) {
    this.log.info('onDataChange', data);
    this.displayData = data;
    this.refreshSelect();
  }

  /**
   * Change the publish version. Re-load the table data according to the publish version.
   * @param version The selected publish version
   */
  changeVersion(version) {
    /**
     * reset source data
     */
    this.selectedVersion = version;
    this.copyData = this.totalData[this.selectedVersion] || [];
    this.data = [...this.copyData];

    /**
     * remove the sort options
     */
    Object.keys(this.sortOptions).forEach(key => {
      this.sortOptions[key] = null;
    });

    /**
     * remove the filter options
     */
    Object.keys(this.filterOptions).forEach(key => {
      this.filterOptions[key].forEach(option => {
        option.selected = false;
      });
    });

    this.search();
  }

  /**
   * Called when the 'select-all' button status changes.
   * @param status The selected status
   */
  selectAll(status) {
    this.log.info('selectAll', status);

    if (status) {
      this.displayData.forEach(item => item.selected = true);
    } else {
      this.displayData.forEach(item => item.selected = false);
    }
    this.refreshSelect();
  }

  /**
   * Batch delete the selected record
   * Simulate the delete action for future usage.
   */
  batchDelete() {
    this.log.info('batchDelete');

    this.isDeleteBtnLoading = true;
    setTimeout(() => {
      this.data.forEach(item => item.selected = false);
      this.refreshSelect();
      this.isDeleteBtnLoading = false;
    }, 1000);
  }

  /**
   * Clear the filter specified by the filter name, pass undefined or null to clear all the filters.
   * @param filterName The filter name
   */
  clearFilter(filterName) {
    this.log.info('clearFilter', filterName);

    if (filterName) {
      this.filterOptions[filterName].forEach(option => {
        option.selected = false;
      });
    } else {
      Object.keys(this.filterOptions).forEach(key => {
        this.filterOptions[key].forEach(option => {
          option.selected = false;
        });
      });
    }
    this.search();
  }

  /**
   * Refresh the select status on the current page.
   */
  refreshSelect() {
    this.log.info('refreshSelect');

    if (this.displayData.length) {
      const isSelectAll = this.displayData.every(item => item.selected);
      const isUnselectAll = this.displayData.every(item => !item.selected);
      this.isSelectAll = isSelectAll;
      this.isSelectIndeterminate = (!isSelectAll) && (!isUnselectAll);
    } else {
      this.isSelectAll = false;
      this.isSelectIndeterminate = false;
    }

    this.isDeleteBtnDisabled = !this.data.some(item => item.selected);
    this.selectCount = this.data.filter(item => item.selected).length;
  }

  /**
   * Sort the table by the column's data
   * @param sortName  The sort name of the column.
   * @param sortValue The sort value which is one of ['descend' 'ascend'].
   */
  sort(sortName, sortValue) {
    this.log.info('sort', sortName, sortValue);

    this.sortName = sortName;
    this.sortValue = sortValue;

    Object.keys(this.sortOptions).forEach(key => {
      if (key === sortName) {
        this.sortOptions[key] = sortValue;
      } else {
        this.sortOptions[key] = null;
      }
    });
    this.search();
  }

  /**
   * Delete the record which specified by the id
   * @param id The unique id
   */
  delete(id: number) {
    this.log.info('delete', id);

    this.isTableLoading = true;
    this.versionMappingService.delete(id).subscribe(
      data => {
        _.remove(this.copyData, item => item.id === id);
        this.data = this.data.filter(item => item.id !== id);
        this.isTableLoading = false;
      },
      err => {
        this.isTableLoading = false;

        this.log.error('delete', err);
        this.msg.error('Failed to delete the record');
      }
    );
  }

  /**
   * Load the data from the remote server
   * @param isReset if set to the first page.
   */
  loadData(isReset = false) {
    this.log.info('loadData', isReset);

    if (isReset) {
      this.pageIndex = 1;
    }
    this.isTableLoading = true;

    this.versionMappingService.list().subscribe(
      data => {
        this.totalData = _.groupBy(data, 'publishVersion');
        this.copyData = this.totalData[this.selectedVersion] || [];
        this.data = [...this.copyData];

        this.filterOptions.operator = _(this.data)
          .map('operator')
          .uniq()
          .map(item => {
            return {name: item, selected: false};
          })
          .value();

        this.isTableLoading = false;
      },
      err => {
        this.isTableLoading = false;
        this.log.error('loadData', err);
        this.msg.error('Failed to load the data');
      }
    );
  }

  /**
   * Re-load the table data according to the current filter and sort options.
   */
  search() {
    this.log.info('search');

    this.filterValue = _.mapValues(this.filterOptions, value =>
      value.filter(item => item.selected).map(item => item.name)
    );

    this.isClearFilterBtnDisabled = _.values(this.filterValue).every(item => item.length === 0);

    const filterFunc = (item) => {
      return Object.keys(this.filterValue).every(key =>
        this.filterValue[key].length === 0 ? true :
          (this.filterValue[key].indexOf(item[key]) !== -1 ? true : false)
      );
    };

    const sortFunc = (a, b) => {
      if (!a[this.sortName]) {
        return (this.sortValue === 'ascend') ? -1 : 1;
      }

      if (!b[this.sortName]) {
        return (this.sortValue === 'ascend') ? 1 : -1;
      }

      if (a[this.sortName] > b[this.sortName]) {
        return (this.sortValue === 'ascend') ? 1 : -1;
      } else if (a[this.sortName] < b[this.sortName]) {
        return (this.sortValue === 'ascend') ? -1 : 1;
      } else {
        return 0;
      }
    };
    this.data = this.copyData.filter(item => filterFunc(item))
      .sort((a, b) => sortFunc(a, b));
  }


  trackById(index: number, versionMappingModel: VersionMappingModel) {
    return versionMappingModel.id;
  }

  test() {

    _.remove(this.copyData, item => item.id === 13);
  }
}
