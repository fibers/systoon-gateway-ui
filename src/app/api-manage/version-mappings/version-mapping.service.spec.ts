import { TestBed, inject } from '@angular/core/testing';

import { VersionMappingService } from './version-mapping.service';

describe('VersionMappingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VersionMappingService]
    });
  });

  it('should be created', inject([VersionMappingService], (service: VersionMappingService) => {
    expect(service).toBeTruthy();
  }));
});
