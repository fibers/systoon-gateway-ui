import {BaseModel} from '../../base/base.model';
export class VersionMappingModel extends BaseModel {
  publishVersion: string;
  serviceId: string;
  apiName: string;
  apiVersion: string;
  openScope: number;
}
