import {Injectable} from '@angular/core';
import {BaseService} from '../../base/base.service';
import {VersionMappingModel} from './version-mapping.model';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class VersionMappingService extends BaseService<VersionMappingModel> {

  constructor(http: HttpClient) {
    super('api/versionMappings', http);
  }

}
