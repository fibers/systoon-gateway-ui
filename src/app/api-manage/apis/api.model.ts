import {BaseModel} from '../../base/base.model';
export class ApiModel extends BaseModel {
  apiName: string;
  apiVersion: string;
  serviceId: string;
  description: string;
}

