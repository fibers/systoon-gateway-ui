import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {Log} from 'ng2-logger';

import {MessageService} from '../../../core/message.service';
import {ApiService} from '../api.service';
import {ApiModel} from '../api.model';
import {ServiceService} from '../../services/service.service';

@Component({
  selector: 'app-api-detail',
  templateUrl: './api-detail.component.html',
  styleUrls: ['./api-detail.component.scss']
})
export class ApiDetailComponent implements OnInit {

  private log = Log.create(this.constructor.name);

  form: FormGroup;
  formTitle = '';
  isEditMode = true;
  isSaveBtnLoading = false;

  allServiceIds = [];
  isServiceOptionLoading = true;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private msg: MessageService,
              private apiService: ApiService,
              private serviceService: ServiceService) {
  }

  /**
   * Initialize the FormGroup object and load the data.
   */
  ngOnInit() {
    this.log.info('ngOnInit', this.route.snapshot.paramMap);

    this.form = this.fb.group({
      id: [null, Validators.nullValidator],
      apiName: [null, [Validators.required]],
      apiVersion: [null, [Validators.required, Validators.pattern('^v[0-9]+$')]],
      serviceId: [null, [Validators.required]],
      description: [null, [Validators.required]]
    });

    const id = +this.route.snapshot.paramMap.get('id');

    if (id) {
      this.formTitle = 'Edit';
      this.isEditMode = true;
      this.apiService.get(id).subscribe(
        data => {
          this.form.patchValue(data);
        },
        err => {
          this.log.error('ngOnInit', err);
          this.msg.error('Failed to load the form data.');
        }
      );
    } else {
      this.formTitle = 'Create';
      this.isEditMode = false;
    }
  }

  /**
   * Load the service list in real-time
   * @param isOpen  The status of the service options
   */
  serviceOptionsOpen(isOpen) {

    this.log.info('serviceOptionOpens', isOpen);

    if (isOpen) {
      this.isServiceOptionLoading = true;
      this.serviceService.list().subscribe(
        data => {
          this.allServiceIds = data.map(item => item.serviceId);
          this.isServiceOptionLoading = false;
        },
        err => {
          this.log.error('showForm', err);
          this.msg.error('Failed to load the version list');
        }
      );
    } else {
      this.isServiceOptionLoading = false;
      this.allServiceIds = [];
    }
  }

  /**
   * Submit the form data to remote server and navigate to the list page.
   * @param apiModel The apiModel object
   */
  submitForm(apiModel: ApiModel) {
    this.log.info('submitForm', apiModel);

    this.isSaveBtnLoading = true;

    if (apiModel.id) {
      this.apiService.update(apiModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    } else {
      this.apiService.create(apiModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    }
  }

  /**
   * Close the form and navigate to the list page.
   */
  closeForm() {
    this.log.info('closeForm');
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  /**
   * The abbreviated word for form.controls.
   * @returns {{[p: string]: AbstractControl}} The form.controls object.
   */
  public get fc() {
    return this.form.controls;
  }

}
