import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../base/base.service';
import {ApiModel} from './api.model';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class ApiService extends BaseService<ApiModel> {

  constructor(http: HttpClient) {
    super('api/apiInfos', http);
  }

  getApiInServiceId(serviceId: string): Observable<ApiModel[]> {
    this.log.info('getApiInServiceId', serviceId);
    return this.http.get<ApiModel[]>(`${this.baseUrl}?serviceId=${serviceId}`);
  }

  getApiByName(apiName: string): Observable<ApiModel[]> {
    this.log.info('getApiByName', apiName);
    return this.http.get<ApiModel[]>(`${this.baseUrl}?apiName=${apiName}`);
  }

}
