import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Log} from 'ng2-logger';

import {ServiceService} from '../service.service';
import {MessageService} from '../../../core/message.service';
import {ServiceModel} from '../service.model';


@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.scss']
})
export class ServiceDetailComponent implements OnInit {

  private log = Log.create(this.constructor.name);

  form: FormGroup;
  formTitle = '';
  isSaveBtnLoading = false;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private msg: MessageService,
              private serviceService: ServiceService) {
  }

  /**
   * Initialize the FormGroup object and load the data.
   */
  ngOnInit() {
    this.log.info('ngOnInit', this.route.snapshot.paramMap);

    this.form = this.fb.group({
      id: [null, Validators.nullValidator],
      serviceId: [null, [Validators.required]],
      serviceName: [null, [Validators.required]],
      description: [null, [Validators.required]]
    });

    const id = +this.route.snapshot.paramMap.get('id');

    if (id) {
      this.formTitle = 'Edit';
      this.serviceService.get(id).subscribe(
        data => {
          this.form.patchValue(data);
        },
        err => {
          this.log.error('ngOnInit', err);
          this.msg.error('Failed to load the form data.');
        }
      );
    } else {
      this.formTitle = 'Create';
    }
  }

  /**
   * Submit the form data to remote server and navigate to the list page.
   * @param serviceModel The serviceModel object
   */
  submitForm(serviceModel: ServiceModel) {
    this.log.info('submitForm', serviceModel);

    this.isSaveBtnLoading = true;

    if (serviceModel.id) {
      this.serviceService.update(serviceModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    } else {
      this.serviceService.create(serviceModel).subscribe(
        data => {
          this.closeForm();
          this.isSaveBtnLoading = false;
        },
        err => {
          this.log.error('submitForm', err);
          this.msg.error('Failed to save the data');
        }
      );
    }
  }

  /**
   * Close the form and navigate to the list page.
   */
  closeForm() {
    this.log.info('closeForm');
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  /**
   * The abbreviated word for form.controls.
   * @returns {{[p: string]: AbstractControl}} The form.controls object.
   */
  public get fc() {
    return this.form.controls;
  }

}
