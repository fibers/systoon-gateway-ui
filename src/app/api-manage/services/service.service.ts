import {Injectable} from '@angular/core';
import {ServiceModel} from './service.model';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../base/base.service';

@Injectable()
export class ServiceService extends BaseService<ServiceModel> {

  constructor(http: HttpClient) {
    super('api/serviceInfos', http);
  }

}
