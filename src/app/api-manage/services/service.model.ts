import {BaseModel} from '../../base/base.model';
export class ServiceModel extends BaseModel {
  serviceId: string;
  serviceName: string;
  description: string;
}

