import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';

import {ApiManageRoutingModule} from './api-manage-routing.module';

import {VersionListComponent} from './versions/version-list/version-list.component';
import {VersionDetailComponent} from './versions/version-detail/version-detail.component';
import {ServiceListComponent} from './services/service-list/service-list.component';
import {ServiceDetailComponent} from './services/service-detail/service-detail.component';
import {ApiListComponent} from './apis/api-list/api-list.component';
import {ApiDetailComponent} from './apis/api-detail/api-detail.component';
import {VersionMappingListComponent} from './version-mappings/version-mapping-list/version-mapping-list.component';
import {VersionMappingDetailComponent} from './version-mappings/version-mapping-detail/version-mapping-detail.component';

import {ApiService} from './apis/api.service';
import {ServiceService} from './services/service.service';
import {VersionService} from './versions/version.service';
import {VersionMappingService} from './version-mappings/version-mapping.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApiManageRoutingModule
  ],
  declarations: [
    VersionListComponent,
    VersionDetailComponent,
    ServiceListComponent,
    ServiceDetailComponent,
    ApiListComponent,
    ApiDetailComponent,
    VersionMappingListComponent,
    VersionMappingDetailComponent
  ],
  providers: [
    ApiService,
    ServiceService,
    VersionService,
    VersionMappingService,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApiManageModule {
}
