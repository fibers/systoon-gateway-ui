import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import {BaseModel} from './base.model';
import {Log} from 'ng2-logger';
import {LoginService} from '../login/login.service';


interface Service<T> {

  list(): Observable<T[]>;
  get(id: number): Observable<T>;
  create(t: T): Observable<T>;
  update(t: T): Observable<T>;
  delete(id: number): Observable<T>;

}

@Injectable()
export abstract class BaseService<T extends BaseModel> implements Service<T> {

  protected log = Log.create(this.constructor.name);

  protected loginService: LoginService;

  constructor(protected baseUrl: string,
              protected http: HttpClient) {
    const inject = Injector.create([{provide: LoginService, deps: []}]);
    this.loginService = inject.get(LoginService);

  }

  list(): Observable<T[]> {
    this.log.info('list');
    return this.http.get<T[]>(this.baseUrl);
  }

  get(id: number): Observable<T> {
    this.log.info('get', id);
    return this.http.get<T>(`${this.baseUrl}/${id}`);
  }

  create(t: T): Observable<T> {
    this.log.info('create', t);
    return this.http.post<T>(this.baseUrl, t, {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  update(t: T): Observable<T> {
    this.log.info('update', t);
    return this.http.patch<T>(`${this.baseUrl}/${t.id}`, t, {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  delete(id: number): Observable<T> {
    this.log.info('delete', id);
    return this.http.delete<T>(`${this.baseUrl}/${id}`);
  }


  // private handleError(operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
  //
  //     this.log.e(`Operation:${operation} Failed: ${error.message}`);
  //     return Observable.of(T);
  //   };
  // }
}
