export class BaseModel {
  id: number;
  operator: string;
  createTime: string;
  updateTime: string;
}
