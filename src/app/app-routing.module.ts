import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth.guard';

import {TestComponent} from './test/test.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {MainComponent} from './main/main.component';


const appRoutes: Routes = [
    {
      path: '', component: MainComponent, canActivate: [AuthGuard], children: [
      {path: 'api-manage', loadChildren: 'app/api-manage/api-manage.module#ApiManageModule'},
      {path: '', redirectTo: 'api-manage', pathMatch: 'full'},
    ]
    },
    {path: 'test', component: TestComponent},
    {path: '**', component: PageNotFoundComponent}
  ]
;


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: true,
        preloadingStrategy: PreloadAllModules
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
