export class User {
  id: number;
  employeeId: string;
  password: string;
  email: string;
  name: string;
  title: string;
  token?: string;
}
