import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {MessageService} from '../core/message.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LogService} from '../core/log.service';
import {Log} from 'ng2-logger';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  private log = Log.create(this.constructor.name);

  loginForm: FormGroup;

  isLoading = false;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private loginService: LoginService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.log.info('ngOnInit');

    this.loginForm = this.fb.group({
      employeeId: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true],
    });
  }

  submitForm(event, value) {
    this.log.info('submitForm', event, value);

    if (!this.loginForm.valid) {

      const controls = this.loginForm.controls;
      for (const controlName of Object.keys(controls)) {
        controls[controlName].markAsDirty();
      }

    } else {

      const employeeId = this.loginForm.get('employeeId').value;
      const password = this.loginForm.get('password').value;

      this.isLoading = true;
      this.loginService.login(employeeId, password)
        .subscribe(
          _ => {
            if (this.loginService.isLoggedIn) {
              this.isLoading = false;
              this.messageService.success('Login Success');

              const redirect = this.loginService.redirectUrl ? this.loginService.redirectUrl : '/';

              // // Set our navigation extras object
              // // that passes on our global query params and fragment
              // let navigationExtras: NavigationExtras = {
              //   queryParamsHandling: 'preserve',
              //   preserveFragment: true
              // };

              // Redirect the user
              this.router.navigate([redirect]);
            } else {
              this.isLoading = false;
              this.log.error('Login Failed');
              this.messageService.error('Login Failed');
            }
          },
          err => {
            this.isLoading = false;
            this.log.error('submitForm', err);
            this.messageService.error('Login Failed');
          }
        );
    }
  }

}
