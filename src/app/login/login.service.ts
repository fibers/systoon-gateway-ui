import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {User} from './user';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoginService {

  private loginUrl = 'api/users';

  isLoggedIn = false;
  currentUser = {};

  redirectUrl: string;

  constructor(private http: HttpClient) {
  }


  login(employeeId: string, password: string): Observable<User> {

    const params = new HttpParams()
      .set('employeeId', employeeId)
      .set('password', password);

    return this.http.get<User>(this.loginUrl, {params: params})
      .map(item => item[0])
      .do(data => {
        if (data) {
          this.currentUser = data;
          this.isLoggedIn = true;
        }
      });
  }

}
