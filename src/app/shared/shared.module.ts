import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgZorroAntdModule} from 'ng-zorro-antd';


// Backup Modules
// const angularMaterialModules: any = [
//   MatAutocompleteModule,
//   MatCheckboxModule,
//   MatDatepickerModule,
//   MatFormFieldModule,
//   MatInputModule,
//   MatRadioModule,
//   MatSelectModule,
//   MatSliderModule,
//   MatSlideToggleModule,
//   MatMenuModule,
//   MatSidenavModule,
//   MatToolbarModule,
//   MatListModule,
//   MatGridListModule,
//   MatCardModule,
//   MatStepperModule,
//   MatTabsModule,
//   MatExpansionModule,
//   MatButtonModule,
//   MatButtonToggleModule,
//   MatChipsModule,
//   MatIconModule,
//   MatProgressSpinnerModule,
//   MatProgressBarModule,
//   MatDialogModule,
//   MatTooltipModule,
//   MatSnackBarModule,
//   MatTableModule,
//   MatSortModule,
//   MatPaginatorModule
// ];

const usedModules = [];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule.forRoot(),
    usedModules,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    usedModules,
  ],
  declarations: []
})
export class SharedModule {
}
