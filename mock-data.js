var faker = require('faker/locale/zh_CN')

const USER_COUNT = 100;
const VERSION_COUNT = 10;
const SERVICE_COUNT = 20;
const API_COUNT = 80;
const VERSION_MAPPING_COUNT = 50;

const users = [];
const versionInfos = [];
const serviceInfos = [];
const apiInfos = [];
const versionMappings = [];

function mockUsers() {


  for (var id = 1; id <= USER_COUNT; id++) {

    var employeeId = id + 100000;
    var passwordSeed = id % 10;
    var password = new Array(7).join(passwordSeed);
    var email = faker.internet.email(faker.name.firstName(), faker.name.lastName, 'syswin.com');
    var name = faker.name.firstName() + faker.name.lastName();
    var title = faker.name.jobTitle();
    var token = faker.random.uuid();

    users.push({
      'id': id,
      'employeeId': employeeId,
      'password': password,
      'email': email,
      'name': name,
      'title': title,
      'token': token
    });
  }

  return users;
}

function mockVersionInfos() {

  for (var id = 1; id <= VERSION_COUNT; id++) {

    var major = parseInt(id / 4) + 1;
    var minor = id % 4;

    var publishVersion = 'v' + major + '.' + minor;
    var description = faker.lorem.sentence();
    var operator = faker.helpers.randomize(['shengyuhong@syswin.com', 'chenshaoao@syswin.com', 'liqiang@syswin.com', 'yuanchangjun@syswin.com']);
    var createTime = faker.date.past();
    var updateTime = faker.date.between(createTime, (new Date).toDateString());

    versionInfos.push({
      'id': id,
      'publishVersion': publishVersion,
      'description': description,
      'operator': operator,
      'createTime': createTime,
      'updateTime': updateTime
    });

  }

  return versionInfos;
}

function mockServiceInfos() {

  for (var id = 1; id <= SERVICE_COUNT; id++) {

    var serviceId = 'systoon-service-id-' + id;
    var serviceName = 'Systoon Service Name ' + id;
    var description = faker.lorem.sentence();
    var operator = faker.helpers.randomize(['shengyuhong@syswin.com', 'chenshaoao@syswin.com', 'liqiang@syswin.com', 'yuanchangjun@syswin.com']);
    var createTime = faker.date.past();
    var updateTime = faker.date.between(createTime, (new Date).toDateString());

    serviceInfos.push({
      'id': id,
      'serviceId': serviceId,
      'serviceName': serviceName,
      'description': description,
      'operator': operator,
      'createTime': createTime,
      'updateTime': updateTime
    });

  }

  return serviceInfos;
}

function mockApiInfos() {

  for (var id = 1; id <= API_COUNT; id++) {

    var serviceIndex = faker.random.number({min: 0, max: SERVICE_COUNT - 1});
    var serviceId = serviceInfos[serviceIndex].serviceId;

    var apiName = 'api' + faker.random.number({min: 1, max: 100}) + '-' + serviceId;
    var apiVersion = 'v' + faker.random.number({min: 1, max: 10});
    var description = faker.lorem.sentence();
    var operator = faker.helpers.randomize(['shengyuhong@syswin.com', 'chenshaoao@syswin.com', 'liqiang@syswin.com', 'yuanchangjun@syswin.com']);
    var createTime = faker.date.past();
    var updateTime = faker.date.between(createTime, (new Date).toDateString());

    apiInfos.push({
      'id': id,
      'apiName': apiName,
      'apiVersion': apiVersion,
      'serviceId': serviceId,
      'description': description,
      'operator': operator,
      'createTime': createTime,
      'updateTime': updateTime
    });

  }

  return serviceInfos;
}

function mockVersionMappings() {

  for (var id = 1; id <= VERSION_MAPPING_COUNT; id++) {

    var versionIndex = faker.random.number({min: 0, max: VERSION_COUNT - 1});
    var apiIndex = faker.random.number({min: 0, max: API_COUNT - 1});

    var version = versionInfos[versionIndex];
    var api = apiInfos[apiIndex];

    var publishVersion = version.publishVersion;
    var serviceId = api.serviceId;
    var apiName = api.apiName;
    var apiVersion = api.apiVersion;
    var openScope = faker.helpers.randomize([1, 2, 3]);
    var operator = faker.helpers.randomize(['shengyuhong@syswin.com', 'chenshaoao@syswin.com', 'liqiang@syswin.com', 'yuanchangjun@syswin.com']);
    var createTime = faker.date.past();
    var updateTime = faker.date.between(createTime, (new Date).toDateString());

    versionMappings.push({
      'id': id,
      'publishVersion': publishVersion,
      'serviceId': serviceId,
      'apiName': apiName,
      'apiVersion': apiVersion,
      'openScope': openScope,
      'operator': operator,
      'createTime': createTime,
      'updateTime': updateTime
    });

  }

  return versionMappings;
}

function generateMockData() {

  mockUsers();
  mockVersionInfos();
  mockServiceInfos();
  mockApiInfos();
  mockVersionMappings();

  return {
    "users": users,
    "versionInfos": versionInfos,
    "serviceInfos": serviceInfos,
    "apiInfos": apiInfos,
    "versionMappings": versionMappings
  };

  // return {
  //   "users": mockUsers(),
  //   "versionInfos": mockVersionInfos(),
  //   "serviceInfos": mockServiceInfos(),
  //   "apiInfos": mockApiInfos(),
  //   "versionMappings": mockVersionMappings()
  // }
}


module.exports = generateMockData;
